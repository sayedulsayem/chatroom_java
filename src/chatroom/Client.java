package chatroom;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {

    static boolean isLoggedIn = false;

    final static int ServerPort = 1234;

    public static void main(String args[]) throws UnknownHostException, IOException {
        Scanner scn = new Scanner(System.in);

        InetAddress ip = InetAddress.getByName("localhost");

        Socket s = new Socket(ip, ServerPort);
        isLoggedIn = true;

        System.out.println("Client Connected to Server");
        System.out.println("Instructions -> ");
        System.out.println("To see all online client type \"online\"");
        System.out.println("To get LogOut from the chat just type \"logout\"");
        System.out.println("For sending message to desire client send it to by this formate \"message#client No\"");

        DataInputStream dis = new DataInputStream(s.getInputStream());
        DataOutputStream dos = new DataOutputStream(s.getOutputStream());

        Thread sendMessage;
        sendMessage = new Thread(new Runnable() {
            @Override
            public void run() {
                String msg = "";
                while (!msg.equals("logout")) {

                    msg = scn.nextLine();

                    try {

                        //System.out.println("send : " + msg);
                        dos.writeUTF(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                try {
                    dos.writeUTF(msg);
                    isLoggedIn = false;
                    dos.close();
                    dis.close();
                    s.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        });

        Thread readMessage = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    if (isLoggedIn == true) {
                        try {

                            String msg = dis.readUTF();
                            System.out.println(msg);
                        } catch (IOException e) {

                            e.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            }
        });

        sendMessage.start();
        readMessage.start();

    }

}
