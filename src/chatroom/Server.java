package chatroom;

import static chatroom.Server.ClientList;
import java.io.*;
import java.util.*;
import java.net.*;

public class Server {

    static ArrayList<ClientHandler> ClientList = new ArrayList<>();

    static int i = 1;

    public static void main(String[] args) throws IOException {

        ServerSocket ss = new ServerSocket(1234);

        System.out.println("Server Started");

        Socket s;

        while (true) {

            s = ss.accept();

            System.out.println("New client request received : " + s);

            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());

            System.out.println("Creating a new handler for client: " + i);

            ClientHandler mtch = new ClientHandler(s, "client " + i, dis, dos);

            Thread t = new Thread(mtch);

            System.out.println("Adding client:" + i + " to active client list");

            ClientList.add(mtch);

            t.start();

            i++;

        }
    }

}

class ClientHandler implements Runnable {

    Scanner scn = new Scanner(System.in);
    private String name;
    final DataInputStream dis;
    final DataOutputStream dos;
    Socket s;
    boolean isloggedin;

    public ClientHandler(Socket s, String name, DataInputStream dis, DataOutputStream dos) {
        this.dis = dis;
        this.dos = dos;
        this.name = name;
        this.s = s;
        this.isloggedin = true;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void run() {

        String received;
        while (true) {
            try {

                received = dis.readUTF();

                System.out.println(received);

                if (received.equals("logout")) {
                    this.isloggedin = false;
                    this.dos.close();
                    this.dis.close();
                    this.s.close();
                    break;
                } else if (received.equals("online")) {

                    dos.writeUTF("Active Client List :");
                    String online = null;
                    String name=this.name;
                    for (ClientHandler str : ClientList) {
                        boolean logIn = str.isloggedin;
                        if (logIn == true) {
                            online = str.getName();
                            if(name.equals(online)){
                                dos.writeUTF(online+"(you)");
                            }
                            else{
                                dos.writeUTF(online);
                            }
                        }
                      
                    }
                } else if (received.contains("#")) {

                    StringTokenizer st = new StringTokenizer(received, "#");
                    String MsgToSend = st.nextToken();
                    String recipient = st.nextToken();

                    for (ClientHandler mc : Server.ClientList) {

                        if (mc.name.equals(recipient) && mc.isloggedin == true) {

                            mc.dos.writeUTF(this.name + " : " + MsgToSend);

                            break;
                        }
                    }

                } else if (received.equals("online")) {

                    dos.writeUTF("Send message to desired client by message#Client No");

                } else {
                    dos.writeUTF("Send message by proper formate accordint to message#Client No");
                }

            } catch (IOException e) {

                e.printStackTrace();
            }

        }

    }
}
